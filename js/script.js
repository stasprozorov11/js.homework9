// Завдання
// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;


const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parent = document.body;

function createList(array, parent){
  const List = document.createElement('ul');
  array.map((array) =>{
    const ListItem = document.createElement('li');
    List.append(ListItem)
    ListItem.append(array);
  });
  parent.append(List);
  return array;
}

let result = createList(array, parent);